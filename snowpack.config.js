const glob = require("glob");

module.exports = {
  mount: {
    "src/templates/entries": "/entries",
    "src/templates/pages": "/pages",
    "src/templates/components": "/components",
    "src/templates/common": "/common",
    "src/templates/hooks": "/hooks",
  },
  plugins: ["@snowpack/plugin-typescript"],
  packageOptions: {
    polyfillNode: true,
  },
  devOptions: {
    port: 3001,
  },
  buildOptions: {
    out: "build",
  },
  alias: {
    typings: "./src/typings",
    templates: "./src/templates",
  },
  optimize: {
    entrypoints: glob.sync("./src/templates/entries/*.tsx"),
    bundle: true,
    minify: true,
    target: "es2018",
  },
};
