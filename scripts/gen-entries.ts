#!/usr/bin/env ts-node-transpile-only

import { writeFile, readdir, stat, mkdir } from "fs/promises";
import { resolve, basename } from "path";

const templatesPath = resolve(__dirname, "../src/templates");
const pagesPath = resolve(templatesPath, "pages");
const entriesPath = resolve(templatesPath, "entries");

run().then(console.log, console.error);

async function run() {
  await ensureEntriesPathExists();
  const names = await getPageNames();
  await Promise.all(names.map(createEntry));
  return "Entries created!";
}

async function ensureEntriesPathExists() {
  try {
    return await stat(entriesPath);
  } catch (e) {
    console.log("Creating the entries folder");
    return await mkdir(entriesPath);
  }
}

async function getPageNames() {
  const files = await readdir(pagesPath);
  return files.map((s) => basename(s));
}

async function createEntry(name: string) {
  const path = resolve(entriesPath, name);
  return await writeFile(
    path,
    `import bootstrap from "templates/common/bootstrap";
import Page from "templates/pages/${basename(name, ".tsx")}";

bootstrap(Page);
  `
  );
}
