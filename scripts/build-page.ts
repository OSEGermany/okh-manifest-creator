#!/usr/bin/env ts-node-transpile-only
import { resolve } from "path";
import { writeFile } from "fs/promises";
import render from "../src/libs/render";
import { getSchema } from "../src/controllers/schema";

const dest = resolve(__dirname, "../build/index.html");

run().then(console.log, console.error);

export async function run() {
  const schema = getSchema();
  const html = render({ template: "index", viewData: { schema } });

  await writeFile(dest, html);

  return "🙌 Page build successfully";
}
