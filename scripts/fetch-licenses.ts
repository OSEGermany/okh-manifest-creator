#!/usr/bin/env ts-node-transpile-only
import fetch from "node-fetch";
import { resolve } from "path";
import { writeFile } from "fs/promises";

const srcUrl =
  "https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json";

const dest = resolve(__dirname, "../src/config/licenses.json");

run().then(console.log, console.error);

async function run() {
  const resp = await fetch(srcUrl);
  const { licenses } = await resp.json();
  const ids = licenses.map((l: any) => l.licenseId);
  await writeFile(dest, JSON.stringify(ids));

  return `📝 Licenses fetched under ${dest}`;
}
