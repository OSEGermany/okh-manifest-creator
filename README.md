# OKH-LOSH-Manifest-Creator

a CI-hosted web interface to create and manipulate manifest files following the OKH-LOSH specification

## Technologies

- [typescript](https://www.typescriptlang.org/)
- [nodejs](https://nodejs.org/en/)
- [koa](https://koajs.com/)
- [react](https://reactjs.org/)

## Run a production server

Run `docker-compose -f docker-compose.production.yml up` to start a production server.

## Run a dev server

Run `docker-compose up` and then `docker-compose exec app npm bundle` to run a dev server on `http://localhost:3000`

## JSON-Schema

The interface is automatically generated using a [JSON-Schema](https://json-schema.org/understanding-json-schema/index.html) file located under `src/config/schema.json`;

The form fields will be rendered in the same order as in the schema file.

> :warning: not all field types are supported. Currently only simple text, enums and number fields are rendered

## Autocomplete

To add autocomplete suggestions to a field define a schema like the following:

```json
"language": {
  "description": "Language as a BCP 47 language tag",
  "type": "string",
  "examples": ["en", "de", "es", "zh"],
  "autocomplete": true
},
```

The examples propertie will be used as suggestions.

## Conditional Fields

The schema will add fields confitionally given some data ist matched.
This is achieved with the followng block on the root.

```json
"allOf": [
    {
      "if": {
        "properties": { "tsdc": { "const": "3DP" } },
        "required": ["tsdc"]
      },
      "then": {
        "properties": {
          "printing-process": {
            "enum": ["FDM", "SLA", "SLS", "MJF", "DMLS"]
          },
          "matrial": {
            "type": "string"
          }
        }
      }
    },
    {
      "if": {
        "properties": { "tsdc": { "const": "PCB" } },
        "required": ["tsdc"]
      },
      "then": {
        "properties": {
          "2d-size-mm": {
            "type": "array",
            "items": {
              "type": "number"
            },
            "minItems": 2,
            "maxItems": 2
          },
          "component-sides": {
            "type": "number"
          }
        }
      }
    }
  ],
```

This is just an attempt to add some coditional logic to the schema.
For the case of tsdc, a better schema structure is needed to be able to fully model the requirements

## Custom Errors

Custom error messages are supported.
Change the error message for a keyword like so:

```json
{
  "type": "object",
  "required": ["foo"],
  "properties": {
    "foo": { "type": "integer" }
  },
  "additionalProperties": false,
  "errorMessage": {
    "type": "should be an object",
    "required": "should have property foo",
    "additionalProperties": "should not have properties other than foo"
  }
}
```

Change the error message for a property like so

```json
{
  "type": "object",
  "required": ["foo", "bar"],
  "allOf": [
    {
      "properties": {
        "foo": { "type": "integer", "minimum": 2 },
        "bar": { "type": "string", "minLength": 2 }
      },
      "additionalProperties": false
    }
  ],
  "errorMessage": {
    "properties": {
      "foo": "data.foo should be integer >= 2",
      "bar": "data.bar should be string with length >= 2"
    }
  }
}
```
