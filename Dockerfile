# pin the version
FROM node:16-alpine

# envs and args
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
ENV PROJECT_ROOT=/srv/app
ENV NODE_PATH=$PROJECT_ROOT/src
ENV PATH=$PROJECT_ROOT/node_modules/.bin:$PATH


# exposing
EXPOSE 1337

# create folder
RUN mkdir -p $PROJECT_ROOT && chown node:node $PROJECT_ROOT

# install deps
RUN npm i -g npm@8.3.0

# less priviliged user
USER node

# let's work
WORKDIR $PROJECT_ROOT

# install deps
COPY --chown=node:node package*.json ./
# --prooduction=false is needed to install devDependencies to build the app
# these will be cleaned afterwards
RUN npm i --also=dev

# copy source code
COPY --chown=node:node . .

RUN npm run build

# cleanup
RUN npm prune --production
RUN npm cache clean --force

# CMD
CMD node -r ts-node/register/transpile-only src/index.ts
