export const fontLink = `
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro:400,500,600,700,900%7CSource+Sans+Pro:400,500,600,900&display=swap" rel="stylesheet">`;
