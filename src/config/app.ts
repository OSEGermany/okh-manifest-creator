import { resolve } from "path";
import { isDevelopment } from "./env";
const { APP_HOST, APP_PORT, PROJECT_ROOT } = process.env;

export const host = APP_HOST || "localhost";
export const port = APP_PORT ? Number(APP_PORT) : 3000;
export const url = isDevelopment ? `http://${host}:${port}` : `https://${host}`;

export const publicPath = resolve(PROJECT_ROOT as string, "public");
export const buildPath = resolve(PROJECT_ROOT as string, "build");
