import { assert } from "libs/utils";
const { NODE_ENV } = process.env;

assert(NODE_ENV, "Please define NODE_ENV");

export default NODE_ENV;
export const isDevelopment = NODE_ENV === "development";
export const isStaging = NODE_ENV === "staging";
export const isProduction = NODE_ENV === "production";
