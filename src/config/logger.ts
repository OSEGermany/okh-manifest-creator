import _ from "lodash";
import { LogLevel } from "consola";

const { LOG_LEVEL = "info" as const } = process.env;

const normalizedLogLevel = _.upperFirst(LOG_LEVEL.trim());

type LogLevelKeys = keyof typeof LogLevel;

function isLogLevel(input: unknown): input is LogLevelKeys {
  return LogLevel[input as any] !== undefined;
}

if (!isLogLevel(normalizedLogLevel)) {
  throw new Error(`LogLevel ${normalizedLogLevel} unknown`);
}

export const level = normalizedLogLevel;
