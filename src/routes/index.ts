import type { Context } from "libs/app";
import router, { routes } from "libs/router";
import { getSchema } from "controllers/schema";

export const enum Routes {
  getIndex = "getIndex",
}

router.get(Routes.getIndex, "/", async (ctx: Context) => {
  const schema = getSchema();
  ctx.render("index", { schema });
});

export default routes;
