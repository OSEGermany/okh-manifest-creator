import { port } from "config/app";
import app from "app";
import logger from "libs/logger";

// Fire it up
run().catch((e) => logger.error(e));

export default async function run() {
  app.listen(port, () => {
    console.log(`\n\n 📜 Manifest Creator is listening to ${port}\n\n`);
  });
}
