import schema from "config/schema.json";
import licenses from "config/licenses.json";

import { merge } from "lodash";

export function getSchema() {
  return merge(schema, {
    $defs: { spdxLicenseExpression: { enum: licenses.sort() } },
  });
}
