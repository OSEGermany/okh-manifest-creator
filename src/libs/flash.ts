import { Context, extendAppContext } from "libs/app";

declare module "libs/app" {
  interface Context {
    flash: typeof flash;
  }
}

export default function init() {
  extendAppContext("flash", flash);
}

// Getter and Setter of flash
function flash(this: Context, data?: Record<string, any>) {
  const ctx = this;
  if (!ctx.session) {
    throw new Error("You need a session middleware for flash to work");
  }
  const sessionFlash = ctx.session.flash || {};

  // return and delete
  if (!data) {
    ctx.session.flash = {};
    return sessionFlash;
  }

  ctx.session.flash = { ...sessionFlash, ...data };
}
