import Router, { UrlOptionsQuery } from "@koa/router";
import type { AppState, Context } from "libs/app";
import compose from "koa-compose";

// cache one instance and export it
const router = new Router<AppState, Context>();
export default router;

// get the url given an Routes enum and params
export function getUrl(name: string, params = {}, options?: UrlOptionsQuery) {
  return router.url(name, params, options);
}

// generate routes middleware
export function routes() {
  return compose([router.routes(), router.allowedMethods()]);
}
