import fs from "fs";

const unsafeChars = new Map([
  ["&", "\\u0026"],
  ["<", "\\u003c"],
  [">", "\\u003e"],
]);

export function safeStringify(obj: any) {
  let s = JSON.stringify(obj);
  for (const [key, value] of unsafeChars) {
    const regexp = new RegExp(key, "g");
    s = s.replace(regexp, value);
  }
  return s;
}

export async function readFile(path: string) {
  return JSON.parse(await fs.promises.readFile(path, "utf-8"));
}

export function readFileSync(path: string) {
  return JSON.parse(fs.readFileSync(path, "utf-8"));
}

export async function writeFile(path: string, data: any) {
  return fs.promises.writeFile(path, JSON.stringify(data, null, 2));
}

export function writeFileSync(path: string, data: any) {
  return fs.writeFileSync(path, JSON.stringify(data, null, 2));
}
