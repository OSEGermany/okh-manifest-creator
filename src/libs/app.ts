import Koa from "koa";

export interface AppState extends Koa.DefaultState {}
export interface Context extends Koa.Context {}
export type Next = () => unknown;

const app = new Koa<AppState, Context>();

export type App = typeof app;

export function extendAppContext(key: string, value: unknown): void {
  app.context[key] = value;
}

export default app;
