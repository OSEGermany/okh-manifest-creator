// get the optional keys of T
export type Optionalkeys<T> = Exclude<
  {
    [k in keyof T]: T[k] extends Exclude<T[k], undefined> ? never : k;
  }[keyof T],
  undefined
>;
// get the required keys of T
export type RequiredKeys<T> = Exclude<keyof T, Optionalkeys<T>>;

export type RequiredOnly<T> = {
  [k in RequiredKeys<T>]: T[k];
};

export type OptionalOnly<T> = {
  [k in Optionalkeys<T>]: T[k];
};

// simplify an object type
export type SimplifyObject<T extends Record<string, unknown>> = {
  [k in keyof T]: T[k];
};
// alias
export type SO<T extends Record<string, unknown>> = {
  [k in keyof T]: T[k];
};

export type ValuesOf<T extends Record<string, unknown>> = T[keyof T];
