import { resolve } from "path";
import { createElement } from "react";
import { renderToString } from "react-dom/server";

const pagesPath = resolve(__dirname, "../templates/pages");

export default async function render(template: string, props: any) {
  const path = resolve(pagesPath, `${template}.tsx`);

  const { default: component } = await import(path);

  return renderToString(createElement(component, props));
}
