import env, { isDevelopment } from "config/env";
import { fontLink } from "config/render";
import { safeStringify } from "libs/json";
import * as App from "libs/app";
import ssr from "libs/ssr";
import logger from "./logger";

// extend the context of the app
declare module "libs/app" {
  interface Context {
    render: typeof contextRender;
    viewData: ViewData;
  }
}

// extend app with render
export function init() {
  App.extendAppContext("render", contextRender);
}

async function contextRender(
  this: App.Context,
  template: string,
  data: any = {}
) {
  const ctx = this;

  const { url, session } = ctx;
  const metadata = { url };
  const flashData = ctx.flash ? ctx.flash() : {};
  const csrfToken = ctx.csrf;
  const viewData = {
    ...data,
    env,
    url,
    csrfToken,
    ...flashData,
  };

  // save a reference down the chain
  ctx.viewData = viewData;

  ctx.type = "html";
  ctx.body = await render({
    metadata,
    viewData,
    template,
    session,
  });
}

export type Metadata = Record<string, any>;
export type ViewData = Record<string, any> & {
  env?: string;
  url?: string;
};

export type RenderOptions = {
  template: string;
  viewData?: ViewData;
  metadata?: Metadata;
  preRender?: string;
  styles?: string;
  session?: App.Context["session"];
};

export async function ssrRender(options: RenderOptions) {
  // skip prerendering for dev
  if (isDevelopment) return render(options);

  const { template, viewData } = options;
  const preRender = await ssr(template, viewData);

  return render({ ...options, preRender });
}

export default function render(options: RenderOptions) {
  const {
    viewData = {},
    metadata = {},
    preRender = "",
    styles = "",
    template,
  } = options;
  const {
    title = "",
    description = "",
    indexable,
    locale,
    url,
    og_title = "",
    og_description = "",
    twitter_description = "",
    og_image = "",
  } = metadata;

  logger.debug(`[render.ts] [render] - ${template}`);

  return `<!DOCTYPE html>
<html lang="${locale}">
<head>
${!indexable ? '<meta name="robots" content="noindex, nofollow">' : ""}
  <title>${title || og_title}</title>

  <meta name="description" content="${description || og_description}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta property="og:title" content="${title || og_title}">
  <meta property="og:description" content="${og_description || description}">
  <meta property="og:locale" content="${locale}">
  <meta property="og:type" content="website">
  <meta property="og:url" content="${url}">
  <meta property="og:image" content="${og_image || ""}">

  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="${og_title || title}">
  <meta name="twitter:description" content="${
    twitter_description || og_description
  }">


  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="512x512" href="/icon-512x512.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="manifest" href="/site.webmanifest">


  <style>${resetCSS}</style>
  ${fontLink}
  ${styles}


</head>
<body>
  <div id="root">${preRender}</div>

  <script id="data" type="application/json">${safeStringify(viewData)}</script>
  <script>window.DATA = JSON.parse(document.querySelector('#data').textContent);</script>

  ${when(
    !isDevelopment,
    `<script src="${assetUrl(`/entries/${template}.js`)}"></script>`
  )}

  ${when(
    isDevelopment,
    `<script>window.HMR_WEBSOCKET_URL = 'ws://localhost:3001';</script>`
  )}

  ${when(
    isDevelopment,
    `<script type="module">
      import bootstrap from "${assetUrl(`/common/bootstrap.js`)}";
      import Page from "${assetUrl(`/pages/${template}.js`)}";
      bootstrap(Page, true);
    </script>`
  )}


</body>
</html>`;
}

// return str when toggle is true
function when(toggle: boolean, str: string) {
  if (!toggle) return "";
  return str;
}

function assetUrl(path: string) {
  return isDevelopment ? `http://localhost:3001${path}` : path;
}

//prettier-ignore
const resetCSS = `
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

html {
  scroll-behavior: smooth;
}
`;
