export function isDefined<T>(v: T | undefined): v is T {
  return v !== undefined;
}

export function isNull(v: unknown): v is null {
  return v === null;
}

export function isUndefined(v: unknown): v is undefined {
  return v === undefined;
}

export function isNumber(v: unknown): v is number {
  return typeof v === "number";
}

export function isString(v: unknown): v is string {
  return typeof v === "string";
}

export function isBoolean(v: unknown): v is boolean {
  return typeof v === "boolean";
}

export function isArray<T>(v: unknown): v is T[] {
  return Array.isArray(v);
}

export function isFunction(v: unknown): v is () => unknown {
  return typeof v === "function";
}

export function isObject(v: unknown): v is Record<string, unknown> {
  return typeof v === "object";
}

export function isPlainObject(v: unknown): v is Record<string, unknown> {
  return isObject(v) && Object.prototype.toString.call(v) === "[object Object]";
}

export function assert<T>(
  x: T | undefined | null,
  msg?: string
): asserts x is T {
  if (isUndefined(x) || isNull(x)) {
    throw new Error(msg || "Assertion failed!");
  }
}

// transform string to base64
export function base64(str: string) {
  return Buffer.from(str).toString("base64");
}

//given an object and a function that takes a key and a value, replace the object values with the function result
export function mapObjectValues<
  I,
  O,
  RI = Record<string, I>,
  RO = Record<string, O>
>(obj: RI, fn: (key: string, value: I) => O): RO {
  const res = {} as RO;
  for (const [key, value] of Object.entries(obj)) {
    // not sure why I need as any here
    (res as any)[key] = fn(key, value);
  }
  return res;
}
