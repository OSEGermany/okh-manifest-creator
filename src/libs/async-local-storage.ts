import { AsyncLocalStorage } from "async_hooks";

const asyncLocalStorage = new AsyncLocalStorage<Store>();

export interface Store {
  [k: string]: any;
}

export function setStore(store: Store) {
  return asyncLocalStorage.enterWith(store);
}

export function getStore() {
  return asyncLocalStorage.getStore();
}
