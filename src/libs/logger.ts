import { isProduction } from "config/env";
import consola, { ConsolaOptions, JSONReporter, LogLevel } from "consola";
import { level } from "config/logger";

const options: ConsolaOptions = {
  level: LogLevel[level],
};

if (isProduction) {
  options.reporters = [new JSONReporter()];
}

export default consola.create(options);
