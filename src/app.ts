import app from "libs/app";
import routes from "routes";
import { init as initRender } from "libs/render";
import staticMiddleware from "middlewares/static";
import requestLocalStorageMiddleware from "middlewares/request-local-storage";

initRender();

app.use(staticMiddleware);
app.use(requestLocalStorageMiddleware);

// routes
app.use(routes());

export default app;
