import type { Context, Next } from "libs/app";

import { setStore } from "libs/async-local-storage";

declare module "libs/async-local-storage" {
  interface Store {
    ctx: Context;
  }
}

export default async function requestLocalStorageMiddleware(
  ctx: Context,
  next: Next
) {
  setStore({ ctx });
  await next();
}
