import serve from "koa-static";
import mount from "koa-mount";
import compose from "koa-compose";
import { buildPath, publicPath } from "config/app";

export default compose([
  mount("/", serve(publicPath)),
  // mount("/", serve(buildPath)),
]);
