import styled from "@emotion/styled";
import React, { ReactNode, useLayoutEffect, useRef, useState } from "react";

export type CollapsibleProps = {
  collapsed?: boolean;
  children: ReactNode;
};

export default function Collapsible(props: CollapsibleProps) {
  const { children, collapsed } = props;

  const [height, setHeight] = useState(collapsed ? "0" : "none");
  const ref = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    if (!ref.current) return;
    const childEl = ref.current;
    if (!childEl) return;

    if (!childEl.parentElement) return;

    childEl.parentElement.style.maxHeight = "none";
    const styles = getComputedStyle(childEl);
    childEl.parentElement.style.maxHeight = "";

    setHeight(
      `calc(${styles.height} + ${styles.marginTop} + ${styles.marginBottom})`
    );
  }, [collapsed]);

  return (
    <Container collapsed={collapsed} style={{ "--max-height": height }}>
      <Inner ref={ref}>{children}</Inner>
    </Container>
  );
}

const Container = styled.div<{ collapsed?: boolean }>`
  max-height: var(--max-height);
  overflow: hidden;
  transition: max-height 0.3s;

  ${({ collapsed }) => collapsed && `max-height: 0;`}
`;

const Inner = styled.div`
  overflow: hidden;
`;
