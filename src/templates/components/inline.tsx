import styled from "@emotion/styled";
import React, { MouseEventHandler, ReactNode } from "react";
import { spacings, Spacings } from "./theme";

type InlineProps = {
  spacing?: Spacings;
  children: ReactNode;
  onClick?: MouseEventHandler;
  as?: string;
};

export default function Inline(props: InlineProps) {
  const { spacing = "m", children, as, ...rest } = props;
  return (
    <Container spacing={spacing} as={as as any} {...rest}>
      {children}
    </Container>
  );
}

const Container = styled.div<{ spacing: Spacings; as?: string }>`
  --spacing: ${({ spacing }) => `${spacings[spacing]}px`};
  display: flex;
  align-items: center;
  gap: var(--spacing);
`;
