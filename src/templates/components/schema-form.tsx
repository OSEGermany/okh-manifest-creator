import React from "react";
import styled from "@emotion/styled";
import type { JSONSchema7 } from "json-schema";
import type { Errors } from "templates/common/utils";
import Group from "./schema-group";

type SchemaFormProps = {
  schema: JSONSchema7;
  data: Record<string, any>;
  onChange: (d: any) => void;
  errors?: Errors;
};

export default function SchemaForm(props: SchemaFormProps) {
  const { schema, data, onChange, errors, ...rest } = props;

  return (
    <Container {...rest}>
      <Group schema={schema} onChange={onChange} data={data} errors={errors} />
    </Container>
  );
}

const Container = styled.div``;
