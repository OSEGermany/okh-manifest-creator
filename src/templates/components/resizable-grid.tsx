import React, { ReactNode, Children, useState, useEffect, useRef } from "react";
import styled from "@emotion/styled";
import { breakpoints, colors, media } from "./theme";
import { Code, Form, Left, Resize, Right } from "./icons";
import { withAttrs } from "templates/common/utils";
import Button from "./button";

type ResizableGridProps = {
  children: ReactNode;
};

export default function ResizableGrid(props: ResizableGridProps) {
  const { children, ...rest } = props;
  const [left, right] = Children.toArray(children);
  const [diff, setDiff] = useState(0);
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const ref = useRef<HTMLDivElement>(null);
  const diffRef = useRef<number>(0);

  let startX = 0;
  let active = false;

  diffRef.current = diff;

  function mouseDown(e: MouseEvent) {
    e.preventDefault();
    const { clientX } = e;
    active = true;
    startX = diffRef.current + clientX;
  }

  function mouseMove(e: MouseEvent) {
    e.preventDefault();
    if (!active) return;
    const { clientX } = e;
    setDiff(startX - clientX);
  }

  function mouseUp(e: MouseEvent) {
    e.preventDefault();
    active = false;
  }

  const onLeftClick = () => {
    setDiff(getParentHalfWidth() - 31);
  };

  const onRightClick = () => {
    setDiff(-getParentHalfWidth() - 31);
  };

  const onLeftTabClick = () => {
    setActiveTabIndex(0);
  };

  const onRightTabClick = () => {
    setActiveTabIndex(1);
  };

  const getParentHalfWidth = () => {
    if (!ref.current) return 0;
    if (!ref.current.parentElement) return 0;

    return ref.current.parentElement.offsetWidth / 2;
  };

  useEffect(() => {
    if (!ref.current) return;

    ref.current.addEventListener("mousedown", mouseDown);
    document.addEventListener("mousemove", mouseMove);
    document.addEventListener("mouseup", mouseUp);

    if (innerWidth < breakpoints.tablet) {
      setDiff(-getParentHalfWidth());
    }

    return () => {
      if (!ref.current) return;
      ref.current.removeEventListener("mousedown", mouseDown);
      document.removeEventListener("mousemove", mouseMove);
      document.removeEventListener("mouseup", mouseUp);
    };
  }, [ref]);

  return (
    <Container {...rest}>
      <Tabs>
        <Tab onClick={onLeftTabClick} secondary={activeTabIndex !== 0}>
          <Form />
        </Tab>
        <Tab onClick={onRightTabClick} secondary={activeTabIndex !== 1}>
          <Code />
        </Tab>
      </Tabs>
      <Inner>
        <LeftCol
          style={{ "--diff": `${-diff}px` }}
          active={activeTabIndex === 0}
        >
          {left}
        </LeftCol>
        <Resizer ref={ref}>
          <ResizeLeft onClick={onLeftClick} />
          <ResizeRight onClick={onRightClick} />
        </Resizer>
        <RightCol
          style={{ "--diff": `${diff}px` }}
          active={activeTabIndex === 1}
        >
          {right}
        </RightCol>
      </Inner>
    </Container>
  );
}

const Container = styled.div``;

const Tabs = styled.div`
  display: none;
  margin: auto;
  background: ${colors.bg};
  padding: 5px 0;

  ${media.mobile} {
    display: flex;
    justify-content: center;
    position: sticky;
    top: 0;
    z-index: 1000;
  }
`;
const Tab = styled(Button)`
  border-color: transparent;
`;

const Inner = styled.div`
  display: flex;
  gap: 20px;
  align-items: stretch;
  margin: 50px -31px;

  ${media.mobile} {
    margin-top: 10px;
    gap: 0;
  }
`;

const Col = styled.div<{ active?: boolean }>`
  // remove the gap and half the resizer width from calculation
  width: calc(50% - 20px - 11px + var(--diff));

  > * {
    overflow: hidden;
  }

  ${media.mobile} {
    width: 100%;
    ${({ active }) => !active && `display: none;`}
  }
`;

const LeftCol = styled(Col)`
  > * {
    margin-left: 31px;
  }
`;

const RightCol = styled(Col)`
  > * {
    margin-right: 31px;
  }
`;

const Resizer = styled.div`
  position: relative;
  position: sticky;
  top: 0;
  padding: 5px 10px;
  cursor: col-resize;
  height: 100vh;

  &:before {
    content: "";
    display: block;
    height: 100%;
    border-left: 2px dashed ${colors.g1};
  }

  &:hover:before {
    border-color: ${colors.g2};
  }

  ${media.mobile} {
    display: none;
  }
`;

const ResizeIcon = styled(Resize)`
  position: absolute;
  top: 30px;
  left: 50%;
  padding: 5px 0;
  transform: translateX(-50%);
  font-size: 35px;
  background: ${colors.bg};
`;

const ResizeLeft = styled(Left)`
  position: absolute;
  top: 0;
  left: 0;
  transform: translateX(-50%);
  font-size: 25px;
  cursor: pointer;
  color: ${colors.g1};

  &:hover {
    color: ${colors.g2};
  }
`;
const ResizeRight = styled(Right)`
  position: absolute;
  top: 0;
  right: 0;
  transform: translateX(50%);
  font-size: 25px;
  cursor: pointer;
  color: ${colors.g1};

  &:hover {
    color: ${colors.g2};
  }
`;
