import styled from "@emotion/styled";
import React, { ReactNode, useState } from "react";
import Modal from "./modal";
import Button from "./button";
import { unmountComponentAtNode, render } from "react-dom";
import { useEffect } from "react";
import { withAttrs } from "templates/common/utils";

export type ConfirmParams = {
  title?: ReactNode;
  okText?: ReactNode;
  cancelText?: ReactNode;
  message: ReactNode;
};

export function confirm(options: ConfirmParams): Promise<boolean> {
  const el = document.createElement("div");
  document.body.appendChild(el);

  return new Promise((resolve) => {
    const clean = () => {
      unmountComponentAtNode(el);
      return el.parentNode && el.parentNode.removeChild(el);
    };
    const resolveAndClean = (res: boolean) => {
      return () => {
        // clean after 1s so css animations are visible
        setTimeout(clean, 1000);
        resolve(res);
      };
    };

    render(
      <Confirm
        {...options}
        onConfirm={resolveAndClean(true)}
        onCancel={resolveAndClean(false)}
      />,
      el
    );
  });
}

export type ConfirmProps = ConfirmParams & {
  active?: boolean;
  onConfirm?: () => void;
  onCancel?: () => void;
};

export function Confirm(props: ConfirmProps) {
  const {
    title,
    message,
    okText = "Ok",
    cancelText = "Cancel",
    onCancel,
    onConfirm,
    active: initialActive = true,
    ...rest
  } = props;

  const [active, setActive] = useState(initialActive);

  const onCancelClick = () => {
    setActive(false);
    onCancel && onCancel();
  };
  const onConfirmClick = () => {
    setActive(false);
    onConfirm && onConfirm();
  };

  useEffect(() => {
    setActive(initialActive);
  }, [initialActive]);

  return (
    <Container
      title={title}
      active={active}
      footer={
        <Buttons>
          {cancelText && (
            <CancelButton onClick={onCancelClick}>{cancelText}</CancelButton>
          )}
          {okText && <OkButton onClick={onConfirmClick}>{okText}</OkButton>}
        </Buttons>
      }
      {...rest}
    >
      <Message>{message}</Message>
    </Container>
  );
}

const Container = styled(Modal)`
  .modal-content {
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
  }
`;
const Message = styled.div``;
const Buttons = styled.div`
  display: flex;
  gap: 6px;
`;

const OkButton = styled(Button)``;

const CancelButton = styled(withAttrs(Button, { secondary: true }))`
  margin-left: auto;
`;
