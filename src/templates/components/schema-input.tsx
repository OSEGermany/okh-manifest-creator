import React, { ChangeEvent, ReactNode } from "react";
import Select from "./select";
import InputText from "./input";
import SchemaRepeatable from "./schema-repeatable";
import styled from "@emotion/styled";
import { Warning } from "./icons";
import { colors } from "./theme";
import Tooltip from "./tooltip";
import InputAutocomplete from "./input-autocomplete";
import type { JSONSchema } from "templates/common/utils";
import SchemaGroup from "./schema-group";

export type SchemaInputProps = {
  name: string;
  schema: JSONSchema;
  required?: boolean;
  value?: any;
  errors?: any;
  onChange: (d: any) => void;
};

export default function SchemaInput(props: SchemaInputProps) {
  const { name, value, required, schema, onChange, errors, ...rest } = props;
  const { type, oneOf } = schema;

  // we found a readonly field
  if (schema.const) {
    return (
      <InputText
        name={name}
        required={required}
        value={schema.const.toString()}
        readOnly
        {...rest}
      />
    );
  }

  // we found an enum select
  if (schema.enum) {
    const options = schema.enum.map((e: any) => {
      return {
        value: e,
        text: e.toString(),
      };
    });

    const onSelectChange = (e: ChangeEvent<HTMLSelectElement>) =>
      onChange(e.target.value);

    return <Select options={options} value={value} onChange={onSelectChange} />;
  }

  // found a oneof select
  if (!type && oneOf) {
    const options = oneOf.map((o: any) => ({
      value: o.const,
      text: o.description,
    }));

    const onSelectChange = (e: ChangeEvent<HTMLSelectElement>) =>
      onChange(e.target.value);

    return <Select options={options} value={value} onChange={onSelectChange} />;
  }

  // found a text input
  if (type === "string") {
    const { format, maxLength, minLength, examples, autocomplete } = schema;
    const shouldAutocomplete = autocomplete && examples;

    if (shouldAutocomplete) {
      return (
        <WithError errors={errors}>
          <InputAutocomplete
            name={name}
            required={required}
            value={value}
            onChange={onChange}
            invalid={!!errors}
            maxLength={maxLength}
            minLength={minLength}
            suggestions={examples as string[]}
            {...rest}
          />
        </WithError>
      );
    }

    const onInputChange = (e: ChangeEvent<HTMLInputElement>) =>
      onChange(e.target.value);

    return (
      <WithError errors={errors}>
        <InputText
          type={formatToInputType(format)}
          name={name}
          required={required}
          value={value}
          onChange={onInputChange}
          invalid={!!errors}
          maxLength={maxLength}
          minLength={minLength}
          {...rest}
        />
      </WithError>
    );
  }

  // found a number input
  if (type === "number" || type === "integer") {
    const onInputChange = (e: ChangeEvent<HTMLInputElement>) =>
      onChange(e.target.value);

    const { minimum, maximum } = schema;

    return (
      <WithError errors={errors}>
        <InputText
          type="number"
          name={"i" + name}
          required={required}
          value={value}
          onChange={onInputChange}
          invalid={!!errors}
          min={minimum}
          max={maximum}
          {...rest}
        />
      </WithError>
    );
  }

  if (type === "object") {
    return (
      <Group>
        <SchemaGroup
          data={value}
          schema={schema}
          errors={errors}
          onChange={onChange}
        />
      </Group>
    );
  }

  // found a repeatable input
  if (type === "array") {
    return <SchemaRepeatable {...props} />;
  }

  return null;
}

type WithErrorProps = {
  errors?: any;
  children: ReactNode;
};

function WithError(props: WithErrorProps) {
  const { children, errors, ...rest } = props;
  const error = errors && errors[0] ? errors[0].message : undefined;

  return (
    <ErrorContainer {...rest}>
      {children}
      {error && (
        <ErrorTooltip message={error}>
          <WarningIcon />
        </ErrorTooltip>
      )}
    </ErrorContainer>
  );
}

function formatToInputType(format?: string) {
  if (format == "email") {
    return "email";
  }

  if (format === "uri") {
    return "url";
  }

  if (format === "date") {
    return "date";
  }

  if (format === "date-time") {
    return "datetime";
  }

  if (format === "time") {
    return "time";
  }

  return "text";
}

const ErrorContainer = styled.div`
  position: relative;
`;
const ErrorTooltip = styled(Tooltip)`
  position: absolute;
  top: 50%;
  right: 10px;
  color: ${colors.y2};
  cursor: pointer;
  transform: translateY(-50%);
`;

const WarningIcon = styled(Warning)`
  font-size: 16px;
`;

const Group = styled.div`
  border: 1px solid ${colors.g1};
  border-radius: 2px;
  padding: 11px;
`;
