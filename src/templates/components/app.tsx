import React, { ReactNode } from "react";
import { Global } from "@emotion/react";
import { GlobalStyles } from "./theme";
import { Container } from "./grid";
import Header from "./header";
import styled from "@emotion/styled";
import Footer from "./footer";

type AppProps = {
  children: ReactNode;
  headerButtons: ReactNode;
};

export default function App(props: AppProps) {
  const { children, headerButtons } = props;
  return (
    <>
      <Global styles={GlobalStyles} />
      <Container>
        <Header buttons={headerButtons} />
        <AppContainer>{children}</AppContainer>
        <Footer />
      </Container>
    </>
  );
}

const AppContainer = styled.div``;
