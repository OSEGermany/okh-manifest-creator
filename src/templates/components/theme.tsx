import { css } from "@emotion/react";

export const fontFamily = {
  secondary: "Source Code Pro,monospace,Courier New",
  primary: "Source Sans Pro,Helvetica,sans-serif",
};

export const colors = {
  b: "rgba(0,0,0,.85)",
  b1: "#4e4d5c",
  b2: "#5087bc",
  r: "red",
  y: "#f6fb6a",
  y1: "#fff963",
  y2: "#e8ec72",
  g1: "#d9d9d9",
  g2: "#678cab",
  g3: "#46749f",
  g4: "#68717a",
  bg: "#ededed",
  w: "#fff",
};

export const spacings = {
  s: 8,
  m: 16,
  l: 32,
};

export type Spacings = keyof typeof spacings;

export const breakpoints = {
  tablet: 768,
  desktop: 1024,
};

export const media = {
  phone: `@media (max-width: ${breakpoints.tablet - 1}px)`,
  tablet: `@media (min-width: ${breakpoints.tablet}px) and (max-width: ${
    breakpoints.desktop - 1
  }px)`,
  // targets both tablets and phones
  mobile: `@media (max-width: ${breakpoints.desktop - 1}px)`,
  // desktop only
  desktop: `@media (min-width: ${breakpoints.desktop}px)`,
};

export const GlobalStyles = css`
  body {
    position: relative;
    font-family: ${fontFamily.primary};
    font-size: 14px;
    color: ${colors.b};
    background-color: ${colors.bg};
    text-rendering: optimizeLegibility;
  }

  * {
    box-sizing: border-box;
  }
`;
