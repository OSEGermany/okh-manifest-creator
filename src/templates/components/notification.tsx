import styled from "@emotion/styled";
import React, { ReactNode, useEffect, useState } from "react";
import { unmountComponentAtNode, render } from "react-dom";
import Portal from "./portal";
import { colors, fontFamily } from "./theme";

export function info(message: ReactNode) {
  const el = document.createElement("div");
  document.body.appendChild(el);

  const clean = () => {
    unmountComponentAtNode(el);
    return el.parentNode && el.parentNode.removeChild(el);
  };

  const onClose = () => setTimeout(clean, 1000);
  render(<Notification onClose={onClose}>{message}</Notification>, el);
}

export type NotificationProps = {
  children: ReactNode;
  onClose: () => void;
};

export default function Notification(props: NotificationProps) {
  const { children, onClose, ...rest } = props;
  const [active, setActive] = useState(false);

  useEffect(() => {
    setActive(true);
    setTimeout(() => {
      setActive(false);
      onClose();
    }, 2000);
  }, [setActive]);

  return (
    <Portal>
      <Container active={active} {...rest}>
        {children}
      </Container>
    </Portal>
  );
}

const Container = styled.div<{ active?: boolean }>`
  position: fixed;
  top: 10px;
  left: 50%;
  background: ${colors.y};
  border-radius: 2px;
  font-family: ${fontFamily.secondary};
  text-align: center;
  font-size: 14px;
  font-weight: 500;
  padding: 10px;
  transform: translate(-50%, -100%);
  box-shadow: 1px 1px 5px ${colors.g1};
  opacity: 0;
  transition: opacity 0.3s, transform 0.3s;

  ${({ active }) =>
    active &&
    `
    transform: translate(-50%, 0);
    opacity: 1;
  `}
`;
