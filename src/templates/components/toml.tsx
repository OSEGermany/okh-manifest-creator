import React, { ChangeEvent } from "react";
import styled from "@emotion/styled";
import { colors, fontFamily } from "./theme";
import { Copy } from "./icons";
import { info } from "./notification";

type TomlProps = {
  content: string;
  onChange: (v: string) => void;
};

export default function Toml(props: TomlProps) {
  const { content, onChange, ...rest } = props;

  const onTextareaChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    onChange(e.target.value);
  };

  const onCopyClick = async () => {
    await navigator.clipboard.writeText(content);
    info("Toml copied to clipboard!");
  };

  return (
    <Container {...rest}>
      <CopyContainer onClick={onCopyClick}>
        <Copy />
      </CopyContainer>
      <Textarea
        value={content}
        onChange={onTextareaChange}
        spellCheck={false}
      />
    </Container>
  );
}

const Container = styled.div`
  position: relative;
`;

const Textarea = styled.textarea`
  font-family: ${fontFamily.secondary};
  border: 1px solid ${colors.g1};
  color: ${colors.b};
  border-radius: 2px;
  font-size: 14px;
  font-weight: 500;
  padding: 11px;
  width: 100%;
  min-height: 100vh;
  line-height: 1.5;
  resize: none;

  &:focus {
    outline: none;
    border-color: ${colors.g2};
  }
`;

const CopyContainer = styled.div`
  padding: 10px;
  position: absolute;
  top: 0;
  right: 0;
  color: ${colors.b1};
  cursor: pointer;

  &:hover {
    color: ${colors.b};
  }
`;
