import styled from "@emotion/styled";
import { media } from "./theme";

export const Container = styled.div`
  max-width: calc(1440px - 8rem);
  margin: auto;
  padding: 0 4rem;

  ${media.mobile} {
    padding: 0 10px;
  }
`;
