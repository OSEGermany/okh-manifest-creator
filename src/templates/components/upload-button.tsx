import styled from "@emotion/styled";
import React, { ChangeEvent } from "react";
import Button from "./button";
import { Upload } from "./icons";
import { media } from "./theme";

type UploadButtonProps = {
  onUpload: (d: string) => void;
};

export default function UploadButton(props: UploadButtonProps) {
  const { onUpload, ...rest } = props;

  const onInputChange = async (e: ChangeEvent<HTMLInputElement>) => {
    const { files } = e.target;
    if (!files) return;

    const file = files.item(0);
    if (!file) return;

    const content = await file.text();
    onUpload(content);
  };

  return (
    <Container as="label" secondary>
      Import
      <Upload />
      <Input
        type="file"
        accept="application/toml, .toml"
        onChange={onInputChange}
      />
    </Container>
  );
}

const Container = styled(Button)`
  ${media.mobile} {
    display: none;
  }
`;
const Input = styled.input`
  display: none;
`;
