import styled from "@emotion/styled";
import React, { ReactNode } from "react";
import { Spacings, spacings } from "./theme";

export type StackProps = {
  spacing?: Spacings;
  children?: ReactNode;
  as?: string;
};

export default function Stack(props: StackProps) {
  const { children, spacing = "m", as, ...rest } = props;

  return (
    <StackContainer spacing={spacing} as={as as any} {...rest}>
      {children}
    </StackContainer>
  );
}

const StackContainer = styled.div<{ spacing: Spacings; as?: string }>`
  --spacing: ${({ spacing }) => `${spacings[spacing]}px`};
  display: flex;
  flex-direction: column;
  gap: var(--spacing);
`;
