import React from "react";
import styled from "@emotion/styled";
import type { JSONSchema7 } from "json-schema";
import { capitalize, withAttrs } from "templates/common/utils";
import Stack from "./stack";
import { colors, fontFamily } from "./theme";
import { marked } from "marked";
import SchemaInput from "./schema-input";

export type SchemaGroupProps = {
  schema: JSONSchema7;
  data?: Record<string, any>;
  onChange: (d: any) => void;
  errors?: any;
};

export default function SchemaGroup(props: SchemaGroupProps) {
  const { schema, data = {}, onChange, errors = {} } = props;

  const { properties, required } = schema;

  if (!properties) return null;

  return (
    <GroupContainer>
      {Object.entries(properties).map(([n, s]) => (
        <Field
          key={"f" + n}
          name={n}
          value={data[n]}
          schema={s as JSONSchema7}
          errors={errors[n] as any}
          onChange={(v: any) => onChange({ ...data, [n]: v })}
          required={Array.isArray(required) ? required.includes(n) : required}
        />
      ))}
    </GroupContainer>
  );
}

type FieldProps = {
  name: string;
  schema: JSONSchema7;
  required?: boolean;
  value: any;
  errors?: any;
  onChange: (d: any) => void;
};

function Field(props: FieldProps) {
  const { name, required, schema } = props;

  const [f, ...rest] = name.split("-");
  const label = [capitalize(f), ...rest].join(" ");

  return (
    <FieldContainer>
      <Label>
        {label}
        {required ? "*" : ""}
      </Label>
      {schema.description && (
        <Description
          dangerouslySetInnerHTML={{ __html: marked.parse(schema.description) }}
        />
      )}
      <SchemaInput {...props} schema={schema} />
    </FieldContainer>
  );
}

const FieldContainer = styled(withAttrs(Stack, { spacing: "s" }))``;

const Label = styled.label`
  font-family: ${fontFamily.secondary};
  font-weight: 600;
  font-size: 14px;
`;

const GroupContainer = styled(Stack)``;
const Description = styled.div`
  line-height: 1.4;
  font-size: 13px;
  font-weight: 500;

  a {
    color: ${colors.b};
  }
`;
