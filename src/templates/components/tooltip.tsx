import styled from "@emotion/styled";
import React, { ReactNode } from "react";
import { colors, fontFamily } from "./theme";

export type TooltipProps = {
  children: ReactNode;
  message: ReactNode;
};

export default function Tooltip(props: TooltipProps) {
  const { children, message, ...rest } = props;

  return (
    <Container {...rest}>
      <Message>{message}</Message>
      {children}
    </Container>
  );
}

const Container = styled.div`
  position: relative;
  display: inline-block;

  &:hover > div {
    visibility: visible;
    opacity: 1;
  }
`;

const Message = styled.div`
  --tooltip-bg: ${colors.y};
  position: absolute;
  bottom: calc(100% + 10px);
  right: -10px;
  padding: 5px;
  border-radius: 2px;
  font-family: ${fontFamily.secondary};
  font-size: 12px;
  width: 150px;
  color: ${colors.b};
  text-align: center;
  line-height: 1.2;
  visibility: hidden;
  background-color: var(--tooltip-bg);
  opacity: 0;
  z-index: 1;
  transition: opacity 0.3s;
  box-shadow: 1px 1px 5px ${colors.g1};

  &::after {
    content: "";
    position: absolute;
    top: 100%;
    right: 12px;
    border-width: 5px;
    border-style: solid;
    border-color: var(--tooltip-bg) transparent transparent transparent;
  }
`;
