import React, { ReactNode } from "react";
import Button from "./button";
import { Download } from "./icons";
import { confirm } from "templates/components/confirm";
import styled from "@emotion/styled";
import { media } from "./theme";

type DownloadButtonProps = {
  content: string;
  children?: ReactNode;
  valid?: boolean;
};

export default function DownloadButton(props: DownloadButtonProps) {
  const { content, valid, ...rest } = props;

  const onClick = async () => {
    if (
      !valid &&
      !(await confirm({
        message: "Are you sure you want to download it anyway?",
        title: "Manifest is invalid",
        okText: "Download",
      }))
    ) {
      return;
    }

    downloadFile(
      "okh.toml",
      `data:text/plain;charset=utf-8,${encodeURIComponent(content)}`
    );
  };

  return (
    <Container as={"a"} onClick={onClick} {...rest}>
      <span>Download</span>
      <Download />
    </Container>
  );
}

function downloadFile(name: string, href: string) {
  var link = document.createElement("a");
  link.href = href;
  link.download = name;
  link.click();
}

const Container = styled(Button)`
  ${media.mobile} {
    width: 100%;
  }
`;
