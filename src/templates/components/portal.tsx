import { Component, ReactNode } from "react";
import { createPortal } from "react-dom";
import { isSSR } from "templates/common/universal";

export type PortalProps = {
  children: ReactNode;
};

const ssr = isSSR();

class Portal extends Component<PortalProps> {
  el: HTMLDivElement;

  constructor(props: PortalProps) {
    super(props);
    this.el = document.createElement("div");
  }

  componentDidMount() {
    document.body.appendChild(this.el);
  }

  componentWillUnmount() {
    document.body.removeChild(this.el as HTMLDivElement);
  }

  render() {
    return createPortal(this.props.children, this.el as HTMLDivElement);
  }
}

export default ssr ? () => null : Portal;
