import React, { useState } from "react";
import type { JSONSchema7 } from "json-schema";
import SchemaInput from "./schema-input";
import styled from "@emotion/styled";
import Stack from "./stack";
import Inline from "./inline";
import Button from "./button";
import { Close, Plus } from "./icons";
import { isJSONSchema, withAttrs } from "templates/common/utils";
import { colors, fontFamily } from "./theme";
import SchemaGroup from "./schema-group";
import { capitalize } from "lodash";
import Collapsible from "./collapsible";

export type SchemaRepeatableProps = {
  name: string;
  schema: JSONSchema7;
  required?: boolean;
  value?: any[];
  errors?: any;
  onChange: (d: any) => void;
};

export default function SchemaRepeatable(props: SchemaRepeatableProps) {
  const { schema, name, errors, required, value = [], onChange } = props;
  const { items, maxItems = Number.POSITIVE_INFINITY } = schema;

  const showAdd = value.length < maxItems;

  if (!isJSONSchema(items)) return null;

  const onValueChange = (v: any, i: number) => {
    const copy = [...value];
    copy[i] = v;
    onChange(copy);
  };

  const onAdd = (d: any) => {
    onChange([...value, d]);
  };

  const onDelete = (i: number) => {
    const copy = [...value];
    copy.splice(i, 1);
    onChange(copy.length ? copy : undefined);
  };

  if (items.type === "object") {
    return (
      <StackContainer>
        {value.map((v, i) => (
          <Group
            key={name + i}
            name={name + " " + i}
            index={i}
            data={v}
            schema={items}
            errors={errors ? errors[i] : undefined}
            onChange={(v) => onValueChange(v, i)}
            onDeleteClick={() => onDelete(i)}
          />
        ))}
        {showAdd && (
          <AddButton onClick={() => onAdd({})}>
            <Plus />
          </AddButton>
        )}
      </StackContainer>
    );
  }

  return (
    <InlineContainer>
      {value.map((v, i) => (
        <InputContainer key={name + i}>
          <Input
            name={name + i}
            schema={items}
            errors={errors ? errors[i] : undefined}
            onChange={(v) => onValueChange(v, i)}
            value={v}
          />
          <Closebutton onClick={() => onDelete(i)}>
            <Close />
          </Closebutton>
        </InputContainer>
      ))}

      {showAdd && (
        <AddButton onClick={() => onAdd("")}>
          <Plus />
        </AddButton>
      )}
    </InlineContainer>
  );
}

type GroupProps = {
  name: string;
  index: number;
  schema: JSONSchema7;
  data: any;
  errors?: any;
  onChange: (d: any) => void;
  onDeleteClick: () => void;
};

function Group(props: GroupProps) {
  const { name, index, onDeleteClick, ...rest } = props;
  const [collapsed, setCollapsed] = useState(false);
  return (
    <GroupContainer>
      <GroupHeadline onClick={() => setCollapsed(!collapsed)}>
        <span>
          {index + 1}. {capitalize(name)}
        </span>{" "}
        <Closebutton onClick={onDeleteClick}>
          <Close />
        </Closebutton>
      </GroupHeadline>
      <Collapsible collapsed={collapsed}>
        <GroupContainerInner>
          <SchemaGroup {...rest} />
        </GroupContainerInner>
      </Collapsible>
    </GroupContainer>
  );
}

const StackContainer = styled(withAttrs(Stack, { spacing: "s" }))``;

const InlineContainer = styled(withAttrs(Inline, { spacing: "s" }))`
  flex-wrap: wrap;
`;

const GroupContainer = styled.div`
  border: 1px solid ${colors.g1};
  border-radius: 2px;
`;

const GroupContainerInner = styled.div`
  padding: 11px;
`;

const GroupHeadline = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  font-family: ${fontFamily.primary};
  font-size: 14px;
  font-weight: 500;
  padding: 0 11px;
  height: 28px;
  cursor: pointer;
  background: ${colors.w};
  gap: 10px;

  &:hover {
    color: ${colors.g3};
    border-color: ${colors.g3};
  }
`;

const Input = styled(SchemaInput)`
  padding-right: 20px;
  width: auto;
  min-width: 50px;
`;
const InputContainer = styled.div`
  dispaly: inline-flex;
  position: relative;
`;

const Closebutton = styled(Button)`
  position: absolute;
  top: 1px;
  bottom: 1px;
  right: 1px;
  padding: 1px 6px;
  cursor: pointer;
  height: auto;
  border: none;
  background: white;
`;

const AddButton = styled(withAttrs(Button, { secondary: true }))``;

// const Chevron = styled(ChevronDown)<{ collapsed?: boolean }>`
//   margin-left: auto;
//   transform: rotate(-180deg);
//   transition: transform 0.4s;

//   ${({ collapsed }) => collapsed && `transform: rotate(0deg);`}
// `;
