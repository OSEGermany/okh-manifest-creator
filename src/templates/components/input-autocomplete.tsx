import styled from "@emotion/styled";
import React, {
  FocusEvent,
  ChangeEvent,
  KeyboardEvent,
  useEffect,
  useRef,
  useState,
} from "react";
import Input, { InputProps } from "./input";
import Portal from "./portal";
import { colors } from "./theme";

export type InputAutocompleteProps = InputProps & {
  onChange: (s: string) => void;
  suggestions: string[];
};

type Position = {
  top: number;
  left: number;
  width: number;
};

export default function InputAutocomplete(props: InputAutocompleteProps) {
  const { suggestions, onChange, ...rest } = props;
  const inputRef = useRef<HTMLInputElement>(null);
  const portalRef = useRef<HTMLUListElement>(null);
  const [cursor, setCursor] = useState(0);
  const [pos, setPos] = useState<Position>({ top: 0, left: 0, width: 0 });
  const [visible, setVisible] = useState(false);

  const filteredSuggestions = filterSuggestions(suggestions, props.value);

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) =>
    onChange(e.target.value);

  const onKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (!suggestions.length) return true;

    if (e.key === "ArrowDown") {
      e.preventDefault();
      setCursor((c) => Math.min(c + 1, suggestions.length));
    }

    if (e.key === "ArrowUp") {
      e.preventDefault();
      setCursor((c) => Math.max(c - 1, 0));
    }

    if (e.key === "Enter") {
      e.preventDefault();
      onChange(suggestions[cursor - 1]);
    }
  };

  useEffect(() => {
    if (!inputRef.current) return;
    const rect = inputRef.current.getBoundingClientRect();

    console.log(rect);

    setPos({
      top: rect.top + rect.height + document.documentElement.scrollTop - 2,
      left: rect.left,
      width: rect.width,
    });
  }, [visible]);

  const checkRelatedTarget = (e: FocusEvent) => {
    return (
      !portalRef.current ||
      !e.relatedTarget ||
      !portalRef.current.contains(e.relatedTarget)
    );
  };

  const onFocus = (e: FocusEvent) => checkRelatedTarget(e) && setVisible(true);
  const onBlur = (e: FocusEvent) => checkRelatedTarget(e) && setVisible(false);

  return (
    <Container onFocus={onFocus} onBlur={onBlur}>
      <Input
        {...rest}
        ref={inputRef}
        onChange={onInputChange}
        onKeyDown={onKeyDown}
      />
      {visible && filteredSuggestions.length > 0 && (
        <Portal>
          <Suggestions
            ref={portalRef}
            invalid={props.invalid}
            pos={pos}
            tabIndex={0}
          >
            {filteredSuggestions.map((s, i) => (
              <SuggestionEl
                key={i}
                onClick={(e) => onChange(s)}
                onMouseEnter={() => setCursor(i + 1)}
                onMouseLeave={() => setCursor(0)}
                active={cursor === i + 1}
              >
                {s}
              </SuggestionEl>
            ))}
          </Suggestions>
        </Portal>
      )}
    </Container>
  );
}

function filterSuggestions(suggestions: string[], value: string = "") {
  const v = value.trim();
  return suggestions.filter((s) => s !== v && s.includes(v));
}

const Container = styled.div`
  position: relative;
`;

const Suggestions = styled.ul<{ invalid?: boolean; pos: Position }>`
  position: absolute;
  background: white;
  box-shadow: 0px 12px 24px rgba(163, 177, 191, 0.35);
  border: 1px solid ${colors.g1};
  border-top: 0;
  border-bottom-left-radius: 2px;
  border-bottom-right-radius: 2px;
  z-index: 100;
  border-color: ${colors.g2};
  visibility: visible;

  ${({ pos }) => `
    top: ${pos.top}px;
    left: ${pos.left}px;
    width: ${pos.width}px;
  `}

  input:invalid + & {
    border-color: ${colors.y1};
  }

  ${({ invalid }) =>
    invalid &&
    `
    input:focus + & {
      border-color: ${colors.y1};
    }
  `}

  &:hover {
    visibility: visible;
  }
`;
const SuggestionEl = styled.li<{ active?: boolean }>`
  cursor: pointer;
  padding: 10px;

  &:hover {
  }

  ${({ active }) =>
    active &&
    `
      background: ${colors.y1};
  `}
`;
