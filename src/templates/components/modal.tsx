import { css } from "@emotion/react";
import styled from "@emotion/styled";
import React, { ReactNode, ReactElement, cloneElement } from "react";
import { unmountComponentAtNode, render } from "react-dom";
import Portal from "./portal";
import { colors, fontFamily, media } from "./theme";

export type ModalProps = {
  title?: ReactNode;
  active?: boolean;
  icon?: ReactNode;
  footer?: ReactNode;
  children: ReactNode;
};

export type showModalArgs = (hideModal: () => void) => ReactElement;

export function showModal(args: showModalArgs): Promise<boolean> {
  const el = document.createElement("div");
  document.body.appendChild(el);

  return new Promise((resolve) => {
    const C = (props: { active: boolean }) => {
      const M = args(resolveAndClean);
      return cloneElement(M, props);
    };

    const clean = () => {
      unmountComponentAtNode(el);
      return el.parentNode && el.parentNode.removeChild(el);
    };

    const resolveAndClean = () => {
      setTimeout(clean, 1000);
      render(<C active={false} />, el);
      resolve(true);
    };

    render(<C active={true} />, el);
  });
}

export default function Modal(props: ModalProps) {
  const { title, icon, children, footer, active, ...rest } = props;
  return (
    <Portal>
      <Container active={active} {...rest}>
        <Inner active={active}>
          <ContentContainer>
            <ContentInner>
              {icon && <Icon>{icon}</Icon>}
              {title && <Title>{title}</Title>}
              <Content>{children}</Content>
            </ContentInner>
            {footer && <Footer>{footer}</Footer>}
          </ContentContainer>
        </Inner>
      </Container>
    </Portal>
  );
}

const Container = styled.div<{ active?: boolean }>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  pointer-events: none;
  visibility: hidden;
  z-index: 5000;

  ${({ active }) =>
    active &&
    css`
      visibility: visible;
      pointer-events: auto;
    `}
`;

const Inner = styled.div<{ active?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  height: 100%;
  width: 100%;
  background: ${colors.b}00;
  transition: background 0.3s;

  ${({ active }) =>
    active &&
    css`
      background: ${colors.b}40;

      > div {
        opacity: 1;
        transform: scale(1);
      }
    `}
`;

const ContentContainer = styled.div`
  max-width: 600px;
  max-height: 80vh;
  min-width: 280px;
  overflow: hidden;
  opacity: 0;
  transform: scale(0.8);
  transition: transform 0.3s, opacity 0.3s;
  margin-left: 10px;
  margin-right: 10px;
  border-radius: 2px;
  background: ${colors.bg};

  ${media.mobile} {
    max-width: calc(100vw - 40px);
    margin: 0;
  }
`;

const ContentInner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 16px;

  ${media.mobile} {
    padding: 10px;
  }
`;

const Title = styled.div`
  font-family: ${fontFamily.secondary};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 1.2;
  margin-bottom: 14px;
`;

const Icon = styled.div`
  margin-bottom: 10px;
`;

const Content = styled.div`
  font-family: ${fontFamily.primary};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 1.2;
  text-align: center;
`;

const Footer = styled.div`
  padding: 10px 16px 16px;

  ${media.mobile} {
    padding: 10px;
  }
`;
