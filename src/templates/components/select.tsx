import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { ChevronDown } from "templates/components/icons";
import React, {
  ChangeEvent,
  ChangeEventHandler,
  useEffect,
  useState,
} from "react";
import { colors, fontFamily } from "./theme";

interface Printable {
  toString: () => string;
}

export type SelectProps<T extends Printable> = {
  id?: string;
  name?: string;
  placeholder?: string;
  options: SelectOption<T>[];
  value?: string;
  defaultValue?: string;
  required?: boolean;
  onChange?: ChangeEventHandler<HTMLSelectElement>;
  dirty?: boolean;
  invalid?: boolean;
};

type SelectOption<T extends Printable> = {
  value: T;
  text: string;
};

export default function Select<T extends Printable>(props: SelectProps<T>) {
  const {
    id,
    name,
    options,
    required,
    placeholder,
    onChange,
    value,
    defaultValue,
    invalid,
    ...rest
  } = props;

  const controlled = value !== undefined;
  const initialValue = controlled ? value : defaultValue;

  const [selected, setSelected] = useState(controlled ? value : defaultValue);

  const onSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    setSelected(e.target.value);
    onChange && onChange(e);
  };

  const selectedText = options.find(
    (o) => o.value.toString() === selected
  )?.text;

  useEffect(() => {
    setSelected(initialValue);
  }, [initialValue]);

  return (
    <Container {...rest}>
      <SelectField
        id={id}
        name={name}
        value={selected}
        onChange={onSelect}
        required={required}
      >
        <SelectOption disabled value="">
          Choose a value
        </SelectOption>
        {options.map((o) => (
          <SelectOption key={o.value.toString()} value={o.value.toString()}>
            {o.text}
          </SelectOption>
        ))}
      </SelectField>

      <SelectPlaceholder hasValue={!!selected} invalid={invalid}>
        {selectedText || placeholder}
      </SelectPlaceholder>
      <IconChevron />
    </Container>
  );
}

const Container = styled.div`
  position: relative;
`;

const SelectField = styled.select`
  cursor: pointer;
  border: 1px solid ${colors.g1};
  border-radius: 2px;
  width: 100%;
  height: 32px;
  line-height: 1.5715;
  padding: 4px 15px;
  opacity: 0;
  position: absolute;
  top: 0;
  left: 0;
`;

const SelectOption = styled.option``;

const SelectPlaceholder = styled.span<{ hasValue: boolean; invalid?: boolean }>`
  font-family: ${fontFamily.primary};
  font-size: 14px;
  height: 32px;
  line-height: 1.5715;
  padding: 4px 15px;
  border: 1px solid ${colors.g1};
  border-radius: 2px;
  cursor: pointer;
  display: block;
  color: ${colors.b};
  background: ${colors.w};

  ${({ hasValue }) =>
    hasValue &&
    css`
      color: ${colors.b};
    `}

  select:focus + & {
    border-color: ${colors.g2};
  }

  ${({ invalid }) =>
    invalid &&
    `
      &,
      &:focus {
        border-color: ${colors.r};
      }
    `}
`;

const IconChevron = styled(ChevronDown)`
  position: absolute;
  right: 10px;
  top: 50%;
  transform: translateY(-50%);
  pointer-events: none;
`;
