import styled from "@emotion/styled";
import React from "react";
import { colors, fontFamily, media } from "./theme";

export default function Footer() {
  return (
    <Container>
      <Text>© OPEN!NEXT</Text>
      <Link
        href="https://dsi.informationssicherheit.fraunhofer.de/en/impressum/losh.opennext.eu/full"
        target="_blank"
      >
        Imprint
      </Link>
      <Link
        href="https://dsi.informationssicherheit.fraunhofer.de/en/dsi/losh.opennext.eu/full"
        target="_blank"
      >
        Data Protection Policy
      </Link>
      <Link
        href="https://gitlab.opensourceecology.de/verein/projekte/okh-losh-manifest-creator/-/issues"
        target="_blank"
      >
        Submit an issue
      </Link>
    </Container>
  );
}

const Container = styled.footer`
  display: flex;
  flex-wrap: wrap;
  padding: 14px 0;
  line-height: 1.5;
  margin-top: 50px;

  ${media.mobile} {
    margin-top: 20px;
  }
`;
const Text = styled.div`
  padding-right: 14px;
  color: ${colors.g4};
`;
const Link = styled.a`
  border-left: 1px solid ${colors.b};
  font-family: ${fontFamily.primary};
  padding: 0 14px;
  color: ${colors.b2};
  text-decoration: none;
`;
