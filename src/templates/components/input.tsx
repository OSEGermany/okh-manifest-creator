import React, {
  ChangeEvent,
  ForwardedRef,
  forwardRef,
  useState,
  ComponentProps,
} from "react";
import type { ChangeEventHandler, KeyboardEventHandler } from "react";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { colors, fontFamily } from "./theme";

export type InputProps = ComponentProps<"input"> & {
  type?: string;
  placeholder?: string;
  name?: string;
  required?: boolean;
  defaultValue?: string;
  value?: string;
  invalid?: boolean;
  dirty?: boolean;
  small?: boolean;
  inline?: boolean;
  autoComplete?: string;
  minLength?: number;
  maxLength?: number;
  min?: number;
  max?: number;
  step?: number;
  pattern?: string;
  onChange?: ChangeEventHandler;
  onKeyDown?: KeyboardEventHandler;
  readOnly?: boolean;
};

export default forwardRef(function Input(
  props: InputProps,
  ref: ForwardedRef<HTMLInputElement>
) {
  const {
    name,
    type = "text",
    placeholder = "",
    required,
    defaultValue,
    invalid = false,
    value,
    onChange,
    ...rest
  } = props;

  const id = `${name}-id`;
  const [dirty, setDirty] = useState(false);

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setDirty(true);
    onChange && onChange(e);
  };

  return (
    <InputField
      ref={ref}
      type={type}
      name={name}
      defaultValue={defaultValue}
      value={value}
      id={id}
      placeholder={placeholder}
      required={required}
      invalid={invalid}
      dirty={dirty}
      onChange={onInputChange}
      {...rest}
    />
  );
});

const InputField = styled.input<{
  type: string;
  invalid: boolean;
  dirty: boolean;
  small?: boolean;
  inline?: boolean;
}>`
  font-family: ${fontFamily.primary};
  border: 1px solid ${colors.g1};
  color: ${colors.b};
  border-radius: 2px;
  font-size: 14px;
  padding: 4px 11px;
  height: 32px;
  width: 100%;
  outline: none;

  ${({ inline }) =>
    inline &&
    `
    display: inline-block;
    width: auto;
  `}

  &::placeholder {
    color: ${colors.b};
  }

  ${({ readOnly }) =>
    readOnly &&
    `
    background: ${colors.bg};
  `}

  ${({ readOnly }) =>
    !readOnly &&
    `
    &:focus {
      border-color: ${colors.g2};
    }
  `}


  ${({ type }) =>
    type === "number" &&
    `
    padding-right: 0;
  `}

  ${({ invalid }) =>
    invalid &&
    css`
      &,
      &:focus {
        border-color: ${colors.y1};
      }
    `}

  ${({ dirty }) =>
    dirty &&
    css`
      &:invalid {
        border-color: ${colors.y1};
      }
    `}
`;
