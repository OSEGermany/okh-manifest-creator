import React, { ReactNode, MouseEventHandler, ElementType } from "react";
import styled from "@emotion/styled";
import { colors, fontFamily } from "./theme";

type ButtonProps = {
  as?: ElementType;
  href?: string;
  download?: string;
  onClick?: MouseEventHandler;
  children?: ReactNode;
  secondary?: boolean;
};

export default function Button(props: ButtonProps) {
  return <Container {...props} />;
}

const Container = styled.div<ButtonProps>`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  gap: 5px;
  font-family: ${fontFamily.secondary};
  font-size: 14px;
  font-weight: 500;
  height: 32px;
  padding: 0 15px;
  border-radius: 2px;
  color: ${colors.b1};
  border: 1px solid ${colors.y1};
  background: ${colors.y1};
  cursor: pointer;
  outline: 0;
  text-decoration: none;

  &:focus,
  &:hover {
    color: ${colors.b};
  }

  ${({ secondary }) =>
    secondary &&
    `
    background: ${colors.w};
    border-color: ${colors.g1};

    &:focus,
    &:hover {
      border-color: ${colors.g2};
    }
  `}
`;
