import { createElement, FunctionComponent } from "react";
import { hydrate, render } from "react-dom";
import { getGlobalProps } from "./universal";

export default function bootstrap(C: FunctionComponent<any>, noSSr?: boolean) {
  const mount = noSSr ? render : hydrate;
  mount(createElement(C, getGlobalProps()), document.getElementById("root"));
}

// HMR
import.meta.hot?.accept();
