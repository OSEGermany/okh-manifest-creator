/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-var-requires */
import type { ViewData } from "libs/render";

declare global {
  interface Window {
    DATA: ViewData;
  }
}

const ssr = isSSR();

export function isSSR() {
  return typeof process !== "undefined";
}

export const getGlobalProps = genUniversalFn(
  () => severLib("async-local-storage").getStore().ctx.viewData as ViewData,
  () => window.DATA
);

function genUniversalFn<T>(server: T, client: T): T {
  return ssr ? server : client;
}

function severLib(path: string) {
  return require("../../libs/" + path);
}
