import React from "react";
import type { JSONSchema7, JSONSchema7Definition } from "json-schema";
import Ajv, { ErrorObject } from "ajv";
import { set, get, assign, merge, findLastIndex } from "lodash";

export const ajv = new Ajv({ strict: false, allErrors: true });
export const ajvStrict = new Ajv({ strict: true, allErrors: true });

export type JSXComponent<T> = (props: T) => React.ReactElement<T, any> | null;

export type JSONSchemaDefinition = JSONSchema7Definition;

export type JSONSchema = JSONSchema7 & {
  propertiesOrder?: string[];
  autocomplete?: boolean;
};

export function withAttrs<
  T,
  A extends Partial<T>,
  R = Omit<T, keyof A> & Partial<A>
>(C: JSXComponent<T>, attrs: A): JSXComponent<R> {
  return function CWithAttrs(props: R) {
    const mergedProps = { ...attrs, ...props } as any as T;
    return <C {...mergedProps} />;
  };
}

export function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function computeSchema(schema: JSONSchema, data: any) {
  const { allOf } = schema;

  if (allOf) {
    return allOf.reduce((acc: JSONSchema, a) => {
      if (!isJSONSchema(a)) return acc;
      if (!isJSONSchema(a.if)) return acc;

      const s = {
        $schema: "http://json-schema.org/draft-07/schema#",
        type: "object",
        ...a.if,
      };
      const valid = ajvStrict.validate(s, data);

      if (valid) {
        return merge({}, acc, a.then);
      }
      return merge({}, acc, a.else);
    }, schema);
  }

  return schema;
}

export function reorderProperties(schema: JSONSchema) {
  const { propertiesOrder, properties } = schema;
  if (!propertiesOrder || !properties) return schema;

  // we reverse the order to take into account wildcards
  const orderRegexes = propertiesOrder.map((o) => new RegExp(o));
  const sortedKeys = Object.keys(properties).sort(
    (a, b) => findOrder(orderRegexes, a) - findOrder(orderRegexes, b)
  );

  const sortedProperties = Object.fromEntries(
    sortedKeys.map((k) => [k, properties[k]])
  );

  return { ...schema, properties: sortedProperties };
}

function findOrder(regexes: RegExp[], key: string) {
  return findLastIndex(regexes, (r) => r.test(key));
}

export function flattenSchema(schema: JSONSchema, root = schema): JSONSchema {
  if (schema.properties) {
    const properties = Object.entries(schema.properties).reduce(
      (acc, [k, v]) => {
        const value = flattenSchema(v as JSONSchema, root);
        return { ...acc, [k]: value };
      },
      {}
    );

    return { ...schema, properties };
  }

  if (schema.items) {
    if (!isJSONSchema(schema.items)) return schema;

    const items = flattenSchema(schema.items, root);

    return { ...schema, items };
  }

  const { $ref, ...rest } = schema;
  const { $defs } = root as any;

  if (!$ref) {
    return schema;
  }

  if (!$defs) {
    return rest;
  }

  const path = $ref.replace("#/$defs/", "");
  const s = { ...rest, ...$defs[path] } as JSONSchema;

  return flattenSchema(s, root);
}

export function getDefaultDataFromSchema(schema: JSONSchema) {
  const { properties } = schema;

  if (!properties) return {};

  return Object.keys(properties).reduce((acc, k) => {
    const s = flattenSchema(properties[k] as JSONSchema, schema);

    if (s.default) {
      return { ...acc, [k]: s.default };
    }
    if (s.const) {
      return { ...acc, [k]: s.const };
    }

    return acc;
  }, {} as Record<string, any>);
}

export function sortDataKeysWithSchema(
  schema: JSONSchema,
  data: Record<string, any>
): Record<string, any> {
  const { properties } = schema;

  if (!properties) {
    return data;
  }

  return Object.keys(properties).reduce((acc, k) => {
    const v = data[k];
    if (v !== undefined) {
      return { ...acc, [k]: v };
    }
    return acc;
  }, {});
}

export type Errors = Record<string, ErrorObject[]>;

export type ErrorMessage = string | Record<string, any>;
export type JSONSchemaWithErrorMessages = JSONSchema & {
  errorMessage?: ErrorMessage;
};

export function customizeError(
  schema: JSONSchemaWithErrorMessages,
  error: ErrorObject
) {
  const { errorMessage } = schema;
  if (!errorMessage) return error;

  if (typeof errorMessage === "string") {
    return extendErrorWithMessage(error, errorMessage);
  }

  const { keyword, instancePath } = error;

  if (errorMessage[keyword]) {
    return extendErrorWithMessage(error, errorMessage[keyword]);
  }

  if (instancePath && errorMessage.properties) {
    const prop = instancePath.split("/").pop() as string;
    if (errorMessage.properties[prop]) {
      return extendErrorWithMessage(error, errorMessage.properties[prop]);
    }
  }

  if (errorMessage._) {
    return extendErrorWithMessage(error, errorMessage._);
  }

  return error;
}

function extendErrorWithMessage(error: ErrorObject, message: string) {
  return { ...error, message };
}

export function normalizeErrors(
  errors: ErrorObject[] | null | undefined,
  schema: JSONSchemaWithErrorMessages,
  data: Record<string, any>
): Errors | undefined {
  if (!errors) return;

  return errors.reduce((acc, e) => {
    const extendedError = customizeError(schema, e);
    const { keyword, params, instancePath } = extendedError;

    let paths: string[] = [];

    if (instancePath) {
      paths = instancePath.split("/").slice(1);
    }

    if (keyword === "required") {
      paths.push(params.missingProperty);
    }

    if (paths.length) {
      const errs = get(acc, paths, []);

      if (Array.isArray(errs)) {
        return set(acc, paths, [...errs, extendedError]);
      }

      return set(acc, paths, assign([extendedError], errs));
    }

    return acc;
  }, {});
}

export function isJSONSchema(
  items: undefined | true | JSONSchemaDefinition | JSONSchemaDefinition[]
): items is JSONSchema {
  if (!items) return false;
  if (typeof items === "boolean") return false;
  if (Array.isArray(items)) return false;
  return true;
}
