import { useState, useRef, useEffect } from "react";

export default function useDrag<T extends HTMLElement>() {
  const [diff, setDiff] = useState(0);
  const ref = useRef<T>(null);
  const diffRef = useRef<number>(0);

  let startX = 0;
  let active = false;

  console.log(diff, diffRef.current);

  diffRef.current = diff;

  function mouseDown(e: MouseEvent) {
    e.preventDefault();
    const { clientX } = e;
    active = true;
    startX = diffRef.current + clientX;
  }

  function mouseMove(e: MouseEvent) {
    e.preventDefault();
    if (!active) return;
    const { clientX } = e;
    setDiff(startX - clientX);
  }

  function mouseUp(e: MouseEvent) {
    e.preventDefault();
    active = false;
  }

  useEffect(() => {
    if (!ref.current) return;

    ref.current.addEventListener("mousedown", mouseDown);
    document.addEventListener("mousemove", mouseMove);
    document.addEventListener("mouseup", mouseUp);

    return () => {
      if (!ref.current) return;
      ref.current.removeEventListener("mousedown", mouseDown);
      document.removeEventListener("mousemove", mouseMove);
      document.removeEventListener("mouseup", mouseUp);
    };
  }, [ref]);

  return { ref, diffRef };
}
