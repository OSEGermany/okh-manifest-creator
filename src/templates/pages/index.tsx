import React, { useMemo, useState } from "react";
import styled from "@emotion/styled";
import App from "templates/components/app";
import SchemaForm from "templates/components/schema-form";
import Toml from "templates/components/toml";
import ResizableGrid from "templates/components/resizable-grid";

import {
  ajv,
  computeSchema,
  flattenSchema,
  getDefaultDataFromSchema,
  JSONSchema,
  normalizeErrors,
  reorderProperties,
  sortDataKeysWithSchema,
} from "templates/common/utils";
import DownloadButton from "templates/components/download-button";
import { stringify, parse } from "@iarna/toml";
import UploadButton from "templates/components/upload-button";
import { media } from "templates/components/theme";

type IndexProps = {
  schema: JSONSchema;
};

export default function Index(props: IndexProps) {
  const { schema: initialSchema } = props;
  const [data, setData] = useState(getDefaultDataFromSchema(initialSchema));
  const validate = useMemo(() => {
    return ajv.compile(initialSchema);
  }, [initialSchema]);

  const onFormChange = (d: any) => {
    setData(d);
  };

  const onTextChange = (t: string) => {
    try {
      const parsed = parse(t);
      setData(parsed);
    } catch (e) {
      // to do throw error for not parsable
    }
  };
  const schema = flattenSchema(
    reorderProperties(computeSchema(initialSchema, data))
  );

  const valid = validate(data);
  const normalizedErrors = normalizeErrors(
    validate.errors,
    initialSchema,
    data
  );

  const tomlContent = stringify(sortDataKeysWithSchema(schema, data));

  return (
    <App
      headerButtons={
        <>
          <UploadButton onUpload={onTextChange} />
          <DownloadButton valid={valid} content={tomlContent} />
        </>
      }
    >
      <ResizableGrid>
        <SchemaForm
          schema={flattenSchema(schema)}
          data={data}
          onChange={onFormChange}
          errors={normalizedErrors}
        />
        <TomlContainer content={tomlContent} onChange={onTextChange} />
      </ResizableGrid>
    </App>
  );
}

const TomlContainer = styled(Toml)`
  position: sticky;
  top: 0;

  ${media.mobile} {
    position: relative;
  }
`;
