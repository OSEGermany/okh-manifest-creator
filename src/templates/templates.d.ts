declare module "csstype" {
  interface Properties {
    [index: string]: any;
  }
}

interface ImportMeta {
  hot: any;
}
